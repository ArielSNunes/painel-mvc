<?php
namespace Models;

use Core\Model;
use Utils\Session;

class Login extends Model
{
	private static $instance;
	public function __construct()
	{
		parent::__construct();
	}
	public static function getInstance()
	{
		if (is_null(self::$instance))
			self::$instance = new self();
		return self::$instance;
	}
	public function login($user, $pass)
	{
		$sql = "SELECT id, user, img, name, role FROM tb_admin_users WHERE user = :u AND password = :p";
		$sql = $this->db->prepare($sql);
		$sql->bindValue(':u', addslashes($user));
		$sql->bindValue(':p', md5($pass));
		$sql->execute();
		if ($sql->rowCount() > 0) {
			Session::set('login', $sql->fetch());
			return true;
		}
		return false;
	}
}