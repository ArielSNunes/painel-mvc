-- Arquivo para guardar os SQL que forem desenvolvidos, não é realmente usado no MVC
drop database if exists painel;
create database painel;
use painel;

create table tb_admin_users (
	id int unsigned not null auto_increment,
	user varchar(255) not null,
	password varchar(32) not null,
	primary key (id)
) engine=InnoDB;

alter table tb_admin_users add img varchar(255);
alter table tb_admin_users add name varchar(100) not null;
alter table tb_admin_users add role int not null;

insert into tb_admin_users (user, password, name, role) values ('admin', md5('admin'),'Ariel Nunes', 2);