; (function (win, doc, $) {
	$(document).ready(function () {
		var $menuBtn = $('div.menu-btn');
		var $sidebar = $('section.menu');
		var $header = $('header.navbar');
		var $content = $('section.home-content');
		var windowSize = window.innerWidth;
		var open = true;
		if (windowSize <= 768) open = false;

		$menuBtn.on('click', function () {
			console.log(open, windowSize);
			windowSize = window.innerWidth;
			if (open) {
				$sidebar.animate({ 'width': 0, 'padding': 0 }, function () {
					open = false;
				});
				$header.animate({ 'left': 0 });
				$content.animate({ 'left': 0 });
				$header.css({ 'width': '100%' });
				$content.css({ 'width': '100%' });
			} else {
				var size = '250px';
				$sidebar.animate({ 'width': size, 'padding': '10px 0' }, function () {
					open = true;
				});
				if (windowSize >= 768) {
					$header.css({ 'width': 'calc(100% - ' + size + ')' });
					$content.css({ 'width': 'calc(100% - ' + size + ')' });
				}
				$sidebar.css('display', 'block');
				$header.animate({ 'left': size });
				$content.animate({ 'left': size });
			}
		});
	});
})(window, document, jQuery)