<section class="menu">
	<div class="menu-wrapper">
		<div class="box-usuario">
			<?php if (isset($viewData['user']['img'])) : ?>
				<div class="imagem-usuario">
					<img src="<?= BASE_URL ?>/uploads/images/<?= $viewData['user']['img'] ?>" alt="$viewData['user']['img']">
				</div>
				<!-- div.imagem-usuario -->
			<?php else : ?>
				<div class="avatar-usuario">
					<ion-icon name="ios-person"></ion-icon>
				</div>
				<!-- div.avatar-usuario -->
			<?php endif; ?>
			<div class="nome-usuario">
				<p><?= $viewData['user']['name'] ?></p>
				<p class="role-usuario"><?= $viewData['user']['role'] ?></p>
			</div>
			<!-- div.nome-usuario -->
		</div>
		<!-- div.box-usuario -->
	</div>
	<!-- div.menu-wrapper -->
</section>
<!-- section.menu -->
<header class="navbar">
	<div class="center">
		<div class="menu-btn">
			<ion-icon name="menu"></ion-icon>
		</div>
		<div class="logout">
			<a href="<?= BASE_URL ?>/sair">
				<ion-icon name="close-circle-outline"></ion-icon>
				<span>Sair</span>
			</a>
		</div>
		<!-- div.logout -->
		<div class="clear"></div>
		<!-- div.clear -->
	</div>
	<!-- div.center -->
	<div class="clear"></div>
		<!-- div.clear -->
</header>
<!-- div.clear -->