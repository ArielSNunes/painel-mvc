<section class="box-login">
	<h2>Efetue o Login</h2>
	<?php if (isset($erro) && !empty($erro)) : ?>
		<div class="erro-box">
			<ion-icon name="close"></ion-icon>
			<?= $erro; ?>
		</div>
		<!-- div.erro-box -->
	<?php endif; ?>
    <form method="POST">
        <input type="text" name="user" id="user" placeholder="Login..." required />
        <input type="password" name="password" id="password" placeholder="Senha..." required />
        <input type="submit" value="Logar" name="loginAction" />
	</form>
	<!-- form -->
</section>
<!-- section.box-login -->