<?php
namespace Controllers;

use Core\Controller;
use Models\Login;
use Utils\Painel;
use Utils\Redirect;
use Utils\Session;

class LoginController extends Controller
{
	public function __construct()
	{
		parent::__construct();
		if (Painel::logado())
			Redirect::toHome();
	}
	public function index()
	{
		$dados = array();
		if (isset($_POST['loginAction'])) {
			if (!empty($_POST['user']) && !empty($_POST['password'])) {
				$user = $_POST['user'];
				$pass = $_POST['password'];
				$l = Login::getInstance();
				if ($l->login($user, $pass)) {
					Redirect::toHome();
				} else {
					$dados['erro'] = 'Usuário ou senha incorretos';
				}
			} else
				$dados['erro'] = 'Usuário ou senha faltando';
		}
		$this->loadTemplate('login', $dados);
	}
}