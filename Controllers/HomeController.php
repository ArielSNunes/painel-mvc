<?php
namespace Controllers;

use Core\Controller;
use Utils\Painel;
use Utils\Redirect;
use Utils\Session;

class HomeController extends Controller
{
	public function __construct()
	{
		parent::__construct();
		if (!Painel::logado())
			Redirect::toLogin();
	}
	public function index()
	{
		$dados = array();
		$user = Session::get('login');
		$user['role'] = Painel::getCargo($user['role']);
		$dados['user'] = $user;
		$this->loadTemplate('home', $dados);
	}
	public function logout()
	{
		Painel::sair();
	}
}