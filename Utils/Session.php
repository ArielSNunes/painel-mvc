<?php
namespace Utils;

class Session
{
	public static function set($name, $data)
	{
		$_SESSION[$name] = $data;
	}
	public static function get($name)
	{
		return (isset($_SESSION[$name]) && !empty($_SESSION[$name])) ? $_SESSION[$name] : null;
	}
	public static function destroy()
	{
		session_destroy();
	}
}