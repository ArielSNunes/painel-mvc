<?php
namespace Utils;

use Models\Login;

class Painel
{
	public static function logado()
	{
		return isset(Session::get('login')['id']) ? true : false;
	}
	public static function sair()
	{
		Session::destroy();
		Redirect::toLogin();
	}
	public static function getCargo($cargo)
	{
		$cargos = [
			0 => 'Usuário',
			1 => 'Sub Admin',
			2 => 'Admin'
		];
		if (!isset($cargos[$cargo])) {
			return $cargos[0];
		}
		return $cargos[$cargo];
	}
}