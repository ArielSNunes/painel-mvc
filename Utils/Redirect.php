<?php
namespace Utils;

class Redirect
{
	private static $url = 'Location: ' . BASE_URL;
	private static $login = 'Location: ' . BASE_URL . '/login';

	public static function toHome()
	{
		header(self::$url);
		die();
	}
	public static function toLogin()
	{
		header(self::$login);
		die();
	}
}